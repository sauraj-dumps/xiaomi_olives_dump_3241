## olive-user 10 QKQ1.191014.001 V12.5.1.0.QCNCNXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: olives
- Brand: Xiaomi
- Flavor: cipher_olives-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.cirrus.20211108.011211
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/olive/olive:10/QKQ1.191014.001/V12.5.1.0.QCNCNXM:user/release-keys
- OTA version: 
- Branch: olive-user-10-QKQ1.191014.001-V12.5.1.0.QCNCNXM-release-keys
- Repo: xiaomi_olives_dump_3241


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
